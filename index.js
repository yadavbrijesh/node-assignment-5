const path = require('path');
const cors = require('cors');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const multer = require('multer');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');

const errorController = require('./controllers/error');
const User = require('./models/user');

// databases
const MONGODB_URI='mongodb+srv://admin:admin@cluster0.xdtso.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'

const store = new MongoDBStore({
    uri: MONGODB_URI,
    collection: 'sessions'
});
const csrfProtection = csrf();

const app = express();

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'resume');
    },
    filename: (req, file, cb) => {
      cb(null, file.originalname);
    }
  });
  
  // to filter only image files
  const fileFilter = (req, file, cb) => {
    //   console.log(file);
    if (
      file.mimetype === 'application/pdf' || 
      file.mimetype === 'application/doc' || 
      file.mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // doc file
    ) {
      cb(null, true);
    } else {
      cb(null, false);
    }
  }  

app.set('view engine', 'ejs');
app.set('views', 'views');

const userRoutes = require('./routes/userRoutes');

app.use(bodyParser.urlencoded({ extended: false }));
// app.use(express.json());
app.use(cors());
app.use(multer({storage: fileStorage, fileFilter: fileFilter}).single('resume'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/resumes', express.static(path.join(__dirname, 'resumes')));
app.use(
    session({
        secret: 'jobPortal secret key',
        resave: false,
        saveUninitialized: false,
        store: store
    })
);
app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {
    res.locals.isAuthenticated = req.session.isLoggedIn;
    res.locals.csrfToken = req.csrfToken();
    next();
});

app.use((req, res, next) => {
    if (!req.session.user) {
        return next();
    }
    User.findById(req.session.user._id)
        .then(user => {
            if (!user) {
                return next();
            };
            req.user = user;
            next();
        })
        .catch(err => {
            next(new Error(err));
        });
});
app.get('/load-session', (req, res, next) => {
    if (!req.session.user) {
        return res.json({ fLoggedIn: false });
        // return next();
    }
    User.findById(req.session.user._id)
        .then(user => {
            if (!user) {
                return next();
            };
            return res.status(200).json({user});
            next();
        })
        .catch(err => {
            next(new Error(err));
        });
});

const companiesRoutes = require('./routes/companiesRoutes');
// app.use('/companies', companiesRoutes);
app.use(companiesRoutes);

app.use(userRoutes);

const authRoutes = require('./routes/auth');
app.use(authRoutes);

app.use('/500', errorController.get500);

app.use(errorController.get404);

app.use((error, req, res, next) => {
    // console.log(error);
    res.status(500).render('500', {
        pageTitle: 'Error!',
        path: '/500',
        isAuthenticated: req.session.isLoggedIn
    });
})

mongoose
    .connect(
        MONGODB_URI, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        retryWrites: true,
    }
    )
    .then(result => {
        const server = app.listen(8080);
        const io = require('./socket').init(server, {
            cors: {
              origin: "http://localhost:3000",
              methods: ["GET", "POST"]
            }
          });
        // console.log(io);
        io.on('connection', socket => {
            console.log('Client connected');
        });
    })
    .catch(err => {
        console.log(err);
    });
