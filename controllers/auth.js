const fs = require('fs');

const io = require('../socket');

const crypto = require('crypto'); // builtin lib in nodejs to build secure random values.

const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const { validationResult } = require('express-validator');

const User = require('../models/user');

const transporter = nodemailer.createTransport(sendgridTransport({
    auth: {
        api_key: "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"
    }
}));

exports.getLogin = (req, res, next) => {
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/login', {
        path: '/login',
        pageTitle: 'Login',
        errorMessage: message,
        oldInput: { email: '', password: '' },
        validationErrors: []
    });
}

exports.getSignup = (req, res, next) => {
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/signup', {
        path: '/signup',
        pageTitle: 'Signup',
        errorMessage: message,
        oldInput: { email: '', password: '', confirmPassword: '' },
        validationErrors: []
    });
};

exports.postLogin = async (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render('auth/login', {
            path: '/login',
            pageTitle: 'Login',
            errorMessage: errors.array()[0].msg,
            oldInput: {
                email,
                password,
            },
            validationErrors: errors.array()
        });
    }

    let user;
    try {
        user = await User.findOne({ email });
    } catch (err) {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }

    if (!user) {
        return res.status(422).render('auth/login', {
            path: '/login',
            pageTitle: 'Login',
            errorMessage: 'Invalid email or password!!',
            oldInput: { email, password },
            validationErrors: []
        });
    }

    let isValidPassword;
    try {
        isValidPassword = await bcrypt.hash(password, user.password);
    } catch (err) {
        console.log(err);
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }

    if (!isValidPassword) {
        return res.status(422).render('/login', {
            path: '/login',
            pageTitle: 'Login',
            errorMessage: 'Invalid email or password!!',
            oldInput: { email, password },
            validationErrors: []
        });
    }
    // req.session.isLoggedIn = true;
    // req.session.user = user;
    req.session.isLoggedIn = true;
    req.session.user = user;
    // console.log(req);
    try {
        await req.session.save();
        // await req.session.save(err => {
        //     // console.log(err);
            
        // });
    } catch (err) {
        // const error = new Error(err);
        // error.httpStatusCode = 500;
        // return next(error);
        res.redirect('/login');
    }

    io.getIO().emit('login', { action: 'login', name: user.firstName })
    res.redirect('/');

    // User.findOne({ email })
    //     .then(user => {
    //         if (!user) {
    //             return res.status(422).render('auth/login', {
    //                 path: '/login',
    //                 pageTitle: 'Login',
    //                 errorMessage: 'Invalid email or password!!',
    //                 oldInput: { email, password },
    //                 validationErrors: []
    //             });
    //         }
    //         bcrypt.compare(password, user.password)
    //             .then(doMatch => {
    //                 if (doMatch) {
    //                     req.session.isLoggedIn = true;
    //                     req.session.user = user;
    //                     return req.session.save(err => {
    //                         // console.log(err);
    //                         res.redirect('/');
    //                     });
    //                 } else {
    //                     return res.status(422).render('/login', {
    //                         path: '/login',
    //                         pageTitle: 'Login',
    //                         errorMessage: 'Invalid email or password!!',
    //                         oldInput: { email, password },
    //                         validationErrors: []
    //                     });
    //                 }
    //             })
    //             .catch(err => {
    //                 // console.log(err);
    //                 res.redirect('/login');
    //             })
    //     })
    //     .catch(err => {
    //         // console.log(err);
    //         const error = new Error(err);
    //         error.httpStatusCode = 500;
    //         return next(error);
    //     });
};

exports.postSignup = async (req, res, next) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const phone = req.body.phone;
    const resume = req.file;
    const password = req.body.password;
    const confirmPassword = req.body.confirmPassword;

    if (!resume) {
        return res.status(422).render('auth/signup', {
            path: '/signup',
            pageTitle: 'Signup',
            errorMessage: 'Please enter the resume file of pdf, doc or docx format!!',
            oldInput: { firstName, lastName, email, phone, resume, password, confirmPassword },
            validationErrors: []
        });
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render('auth/signup', {
            path: '/signup',
            pageTitle: 'Signup',
            errorMessage: errors.array()[0].msg,
            oldInput: { firstName, lastName, email, phone, password, confirmPassword },
            validationErrors: errors.array()
        });
    }

    let hashedPassword;
    try {
        hashedPassword = await bcrypt.hash(password, 12);
    } catch (err) {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }
    // const resumeUrl = resume.originalname;
    const resumeUrl = resume.path;

    const user = new User({
        firstName, lastName, email, phone, resumeUrl, password: hashedPassword
    });

    try {
        await user.save();
    } catch (err) {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }
    res.redirect('/login');

    console.log(email);
    // error may be seen in this step as cannot set headers after they are sent.. In this case, you called res.redirect(), which caused the response to become Finished. Then this code threw an error (res.req is null). and since the error happened within this actual function(req, res, next) (not within a callback), Connect was able to catch it and then tried to send a 500 error page. But since the headers were already sent, Node.js's setHeader threw the error that you saw.
    try {
        await transporter.sendMail({
            to: email,
            from: 'karnabibek96@gmail.com',
            subject: 'Signup succeeded',
            html: '<h1>You have signed up successfully!!</h1>'
        })
    } catch (err) {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }

    // bcrypt.hash(password, 12)
    //     .then(hashedPassword => {
    //         const user = new User({
    //             firstName, lastName, email, phone, password: hashedPassword
    //         });
    //         return user.save();
    //     })
    //     .then(result => {
    //         res.redirect('/login');
    //         return transporter.sendMail({
    //             to: email,
    //             from: 'karnabibek96@gmail.com',
    //             subject: 'Signup succeeded',
    //             html: '<h1>You have signed up successfully!!</h1>'
    //         })
    //     })
    //     .catch(err => {
    //         const error = new Error(err);
    //         error.httpStatusCode = 500;
    //         return next(error);
    //     })
}

exports.postLogout = (req, res, next) => {
    req.session.destroy(err => {
        console.log(err);
        res.redirect('/');
    });
};