const express = require('express');

const router = express.Router();

const companiesController = require('../controllers/companies-controller');

// for resume
const userController = require('../controllers/userControllers');

const isAuth = require('../middleware/is-Auth');

const bodyParser = require('body-parser').json();

// get users
router.get('/', companiesController.getCompanies);

// add company
router.get('/add-company', isAuth, companiesController.getAddCompany);

router.post('/add-company', isAuth, bodyParser, companiesController.addCompany);


// edit and delete are not available in the views
// edit company
router.post('/edit-company', isAuth, bodyParser, companiesController.editCompany);

// delete company
router.post('/delete-company', isAuth, bodyParser, companiesController.deleteCompany);

router.get('/resume', isAuth, userController.getResume);

router.get('/update-resume', isAuth, userController.getUpdateResume);

router.post('/update-resume', isAuth, userController.postUpdateResume);

module.exports = router;